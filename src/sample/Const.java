package sample;

public class Const {
    public static final String DISCIPLINE_TABLE = "discipline";
    public static final String ID_DISCIPLINE = "id_discipline";
    public static final String DISCIPLINE = "discipline";

    public static final String QUESTION_TABLE = "question";
    public static final String ID_QUESTION = "id_question";
    public static final String QUESTION = "question";

    public static final String TEACHER_TABLE = "teachers";
    public static final String ID_TEACHER = "id_teachers";
    public static final String TEACHER = "teachers";

    public static final String TICKETS_TABLE = "tickets";
    public static final String ID_TICKETS = "id_tickets";
    public static final String ID_QUESTION_ONE = "id_question_one";
    public static final String ID_QUESTION_TWO = "id_question_two";
    public static final String ID_QUESTION_THREE = "id_question_three";
    public static final String ID_DISCIPLINE_TICKET = "id_discipline_ticket";
    public static final String ID_TEACHER_TICKET = "id_teacher_ticket";
}
