package sample.controllers;

import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.DataBaseHandler;

public class QuestionAddController {

    @FXML
    private TextField questionAddText;

    @FXML
    private Button questionAddButton;

    @FXML
    private Button back_questionAdd;

    @FXML
    void initialize() {
        back_questionAdd.setOnAction(event -> {
            back_questionAdd.getScene().getWindow().hide();

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/sample/fxmls/sample.fxml"));

            try {
                fxmlLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent parent = fxmlLoader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(parent));
            stage.showAndWait();
        });

        DataBaseHandler dataBaseHandler = new DataBaseHandler();

        questionAddButton.setOnAction(event -> {
            try {
                dataBaseHandler.addQuestion(questionAddText.getText());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        });

    }
}
