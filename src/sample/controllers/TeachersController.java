package sample.controllers;

import java.io.IOException;
import java.sql.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.DataBaseHandler;

public class TeachersController {

    @FXML
    private Button back_teacher;


    @FXML
    private TextField AddDelete;

    @FXML
    private Button DeleteButton;

    @FXML
    private Button UpdateButton;

    @FXML
    private TextField UpdateTextId;

    @FXML
    private TextField UpdateText;

    @FXML
    private ListView<String> test;




    @FXML
    void initialize() {
        back_teacher.setOnAction(event -> {
            back_teacher.getScene().getWindow().hide();

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/sample/fxmls/sample.fxml"));

            try {
                fxmlLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent parent = fxmlLoader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(parent));
            stage.showAndWait();
        });

        DataBaseHandler dataBaseHandler = new DataBaseHandler();

        try {
            resultTeachers();
        } catch (Exception e) {
            e.printStackTrace();
        }


        DeleteButton.setOnAction(event -> {
            try {
                dataBaseHandler.teacherDeleteButton(AddDelete.getText());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        });

        UpdateButton.setOnAction(event -> {
            try {
                dataBaseHandler.updateTeacherButton(UpdateText.getText(),UpdateTextId.getText());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        });

    }

    public void resultTeachers() throws Exception {

        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        String url = "jdbc:mysql://localhost:3306/" + "artproject" + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");
        ObservableList listtest = FXCollections.observableArrayList();

        try {

            Connection con = DriverManager.getConnection(url, "root", "phnc7nn3");
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM teachers");
            int columns = resultSet.getMetaData().getColumnCount();
            System.out.println(columns);
            System.out.println("Select ok");
            while (resultSet.next()) {
                listtest.add("id: " + resultSet.getString(1) + " " + "Преподователь: " + resultSet.getString(2));
            }

            test.getItems().addAll(listtest);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
