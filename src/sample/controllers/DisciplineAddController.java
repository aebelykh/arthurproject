package sample.controllers;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.DataBaseHandler;


public class DisciplineAddController {

    @FXML
    private TextField disciplineAddText;

    @FXML
    private Button disciplineAddButton;

    @FXML
    private Button back_disciplineAdd;

    @FXML
    void initialize() {

        back_disciplineAdd.setOnAction(event -> {
            back_disciplineAdd.getScene().getWindow().hide();

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/sample/fxmls/sample.fxml"));

            try {
                fxmlLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent parent = fxmlLoader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(parent));
            stage.showAndWait();
        });

        DataBaseHandler dataBaseHandler = new DataBaseHandler();

        disciplineAddButton.setOnAction(event -> {
            try {
                dataBaseHandler.addDiscipline(disciplineAddText.getText());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        });
    }
}
