package sample.controllers;

import java.io.IOException;
import java.sql.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.stage.Stage;
import sample.DataBaseHandler;

public class TicketsAddController {
    @FXML
    private Button ticketsAddButton;

    @FXML
    private Button back_ticketAdd;

    @FXML
    private ChoiceBox<String> id_discipline_ticket;

    @FXML
    private ChoiceBox<String> id_teacher_ticket;

    @FXML
    private ChoiceBox<String> id_question_one;

    @FXML
    private ChoiceBox<String> id_question_two;

    @FXML
    private ChoiceBox<String> id_question_three;

    @FXML
    void initialize() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        back_ticketAdd.setOnAction(event -> {
            back_ticketAdd.getScene().getWindow().hide();

            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/sample/fxmls/sample.fxml"));

            try {
                fxmlLoader.load();
            } catch (IOException e) {
                e.printStackTrace();
            }

            Parent parent = fxmlLoader.getRoot();
            Stage stage = new Stage();
            stage.setScene(new Scene(parent));
            stage.showAndWait();
        });

        DataBaseHandler dataBaseHandler = new DataBaseHandler();

        discipline();
        teacher();
        question();

        ticketsAddButton.setOnAction(event -> {
            try {
                dataBaseHandler.addTickets(id_discipline_ticket.getValue(),id_teacher_ticket.getValue(),id_question_one.getValue(), id_question_two.getValue(), id_question_three.getValue());
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        });


    }

    private void discipline() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        String url = "jdbc:mysql://localhost:3306/" + "artproject" + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");
        ObservableList listtest = FXCollections.observableArrayList();

        try {

            Connection con = DriverManager.getConnection(url, "root", "phnc7nn3");
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM discipline");
            int columns = resultSet.getMetaData().getColumnCount();
            System.out.println(columns);
            System.out.println("Select ok");
            while (resultSet.next()) {
                listtest.add(resultSet.getString(2));
            }

            id_discipline_ticket.getItems().addAll(listtest);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    private void teacher() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        String url = "jdbc:mysql://localhost:3306/" + "artproject" + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");
        ObservableList listtest = FXCollections.observableArrayList();

        try {

            Connection con = DriverManager.getConnection(url, "root", "phnc7nn3");
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM teachers");
            int columns = resultSet.getMetaData().getColumnCount();
            System.out.println(columns);
            System.out.println("Select ok");
            while (resultSet.next()) {
                listtest.add(resultSet.getString(2));
            }

            id_teacher_ticket.getItems().addAll(listtest);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void question() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
        String url = "jdbc:mysql://localhost:3306/" + "artproject" + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");
        ObservableList listtest = FXCollections.observableArrayList();

        try {

            Connection con = DriverManager.getConnection(url, "root", "phnc7nn3");
            Statement stmt = con.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM question");
            int columns = resultSet.getMetaData().getColumnCount();
            System.out.println(columns);
            System.out.println("Select ok");
            while (resultSet.next()) {
                listtest.add(resultSet.getString(2));
            }

            id_question_one.getItems().addAll(listtest);
            id_question_two.getItems().addAll(listtest);
            id_question_three.getItems().addAll(listtest);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
