package sample;

import java.sql.*;

public class DataBaseHandler extends Configs {

    public void getDbConnection() throws ClassNotFoundException, SQLException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("INSERT INTO discipline (id_discipline,discipline) VALUES ('4', 'fghfghn')");
            System.out.println("Insert ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addDiscipline(String discription) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("INSERT INTO discipline (discipline) VALUES ('" + discription + "')");
            System.out.println("Insert ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addTeachers(String teachers) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("INSERT INTO teachers (teachers) VALUES ('" + teachers + "')");
            System.out.println("Insert ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addQuestion(String question) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("INSERT INTO question (question) VALUES ('" + question + "')");
            System.out.println("Insert ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void addTickets(String disciline_ticket, String teacher_ticket, String question_one, String question_two, String question_three) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("INSERT INTO tickets (discipline_ticket,teacher_ticket,question_one,question_two,question_three) VALUES ('" + disciline_ticket + "','" + teacher_ticket + "','" + question_one + "','" + question_two + "','" + question_three + "')");
            System.out.println("Insert ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void disciplineDeleteButton(String disciplinetext) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("DELETE FROM discipline WHERE id_discipline='" + disciplinetext + "';");
            System.out.println("Delete ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void questionDeleteButton(String questiontext) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("DELETE FROM question WHERE id_question='" + questiontext + "';");
            System.out.println("Delete ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void teacherDeleteButton(String teachertext) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("DELETE FROM teachers WHERE id_teachers='" + teachertext + "';");
            System.out.println("Delete ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void ticketDeleteButton(String tickettext) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("DELETE FROM tickets WHERE id_tickets='" + tickettext + "';");
            System.out.println("Delete ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateDisciplineButton(String discipline, String id_discipline) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("UPDATE discipline SET discipline='" + discipline + "' WHERE id_discipline='" + id_discipline + "'");
            System.out.println("Delete ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateTeacherButton(String teacher, String id_teacher) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("UPDATE teachers SET teachers='" + teacher + "' WHERE id_teachers='" + id_teacher + "'");
            System.out.println("Delete ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateQuestionButton(String question, String id_question) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

        String url = "jdbc:mysql://localhost:3306/" + dbName + "?" + "autoReconnect=true&useSSL=false&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";                      //вместо локалхост свой путь к базе
        System.out.println("Connect to driver");

        try {

            Connection con = DriverManager.getConnection(url, dbUser, "phnc7nn3");
            Statement stmt = con.createStatement();
            stmt.executeUpdate("UPDATE question SET question='" + question + "' WHERE id_question='" + id_question + "'");
            System.out.println("Delete ok");
            stmt.close();
            con.close();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }


}






